# Shell Script Collections

在資料整理過程中撰寫的Shell腳本，用於提取數據。

## TOC

1. [Hong Kong Motion Picture Industry Association](#hong-kong-motion-picture-industry-association)  
2. [Hong Kong File Critics Society](#hong-kong-file-critics-society)  
2.1 [HKinema](#hkinema)  
3. [Hong Kong Film Archive](#hong-kong-film-archive)  
3.1 [Newsletter](#newsletter)  
3.2 [Hong Kong Filmmakers](#hong-kong-filmmakers)  
3.3 [Hong Kong Film Search](#hong-kong-film-search)  
4. [Change Log](#change-log)  


To remove `^M` in html source, using `s/\r$//g` in command `sed`.

## Hong Kong Motion Picture Industry Association

香港影業協會

```bash
official_site='http://mpia.org.hk/content/links.php'
download_tool='curl -fsSL'  # or 'wget -qO-'

${download_tool} "${official_site}" | sed -r -n '/class="unit"/,/<\/ul>/{/<li>/{s@.*href="([^"]+)"[^>]+>[[:space:]]*([^<]+)<[^>]+>[[:space:]]*([^<]+).*$@\1|\2|\3@g;s@[()]@@g;p}}' | awk -F\| '{printf("* [%s](%s \"%s\") (%s)\n",$2,$1,$3,$3)}'
```


## Hong Kong File Critics Society

香港電影評論協會

### HKinema

提取各期名稱及對應下載鏈接。

```bash
# Date: Aug 14, 2021 Sat 14:40 ET

official_site='https://www.filmcritics.org.hk'
save_dir="/tmp/HKinema"
save_path="${save_dir}/HKinemaList.txt"

[[ -d "${save_dir}" ]] || mkdir -p "${save_dir}"
> "${save_path}"

download_tool='curl -fsSL'  # or 'wget -qO-'

# https://www.filmcritics.org.hk/taxonomy/term/3/1
# https://www.filmcritics.org.hk/taxonomy/term/3/1?page=0
release_page="${official_site}/taxonomy/term/3/1"

page_count=$(${download_tool} "${release_page}" 2> /dev/null | sed -r -n '/pager-last/{s@.*href="([^"]+)".*@\1@g;s@^[^=]+=@@g;p}')

seq 0 "${page_count}" | while read -r num; do
    release_info=$(${download_tool} "${release_page}?page=${num}" 2> /dev/null | sed -r -n '/listing-article/,/<\/article>/{s/\r$//g;/^$/d;/^《HKinema.*/{s@.*@&\n-----@g};p}' | sed -r -n '/listing-article/,/<\/article>/{/-----/{n;s@（.*$@@g;s@^.*?：@@g;p}; /<img.*/{s@.*data-src="([^"]+)".*@\1@g;s@^.*?[^[:digit:]]+([[:digit:]]+)\..*?@\1|&@g;p}; /article-title/{s@.*href="([^"]+)"[^>]*>[[:space:]]*([^<]+).*@\1@g;p}; /published-date/{s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@.*@&---@g;p}}' | sed ':a;N;$!ba;s@\n@|@g' | sed -r -n 's@-{2,}\|?@\n@g;p' | sed '/^$/d')

    # 54|/sites/default/files/2021-06/hkinema54.jpg|/zh-hant/node/2903|合拍，唔合拍|2021-06-02
    # 01|/sites/default/files/hkinema01.jpg|/zh-hant/%E5%AD%B8%E6%9C%83%E5%87%BA%E7%89%88/hkinema/%E3%80%8Ahkinema%E3%80%8B%E5%89%B5%E5%88%8A%E8%99%9F|消失中的電影文化|2007-12-30

    while IFS="|" read -r r_num img_link intro_link r_title r_date; do
        prefix_str="第${r_num}號"
        [[ "${r_num}" == '01' ]] && prefix_str='創刊號'

        pdf_link="${official_site}/hkinema/hkinema${r_num}.pdf"  # https://www.filmcritics.org.hk/hkinema/hkinema01.pdf

        echo "${r_num}|${prefix_str}：${r_title}|${r_date}|${official_site}${intro_link}|${official_site}${img_link}|${pdf_link}" >> "${save_path}"
        # 54|第54號：合拍，唔合拍|2021-06-02|https://www.filmcritics.org.hk/zh-hant/node/2903|https://www.filmcritics.org.hk/sites/default/files/2021-06/hkinema54.jpg|https://www.filmcritics.org.hk/hkinema/hkinema54.pdf
        # 01|創刊號：消失中的電影文化|2007-12-30|https://www.filmcritics.org.hk/zh-hant/%E5%AD%B8%E6%9C%83%E5%87%BA%E7%89%88/hkinema/%E3%80%8Ahkinema%E3%80%8B%E5%89%B5%E5%88%8A%E8%99%9F|https://www.filmcritics.org.hk/sites/default/files/hkinema01.jpg|https://www.filmcritics.org.hk/hkinema/hkinema01.pdf

    done <<< "${release_info}"

    sleep 1
done


# - makrdown format output
# without cover image
awk -F\| 'BEGIN{printf("期號|日期|下載鏈接\n---|---|---\n")}{printf("[%s](%s)|%s|[PDF](%s)\n",$2,$4,$3,$5)}' "${save_path}"

# with cover image
# https://www.filmcritics.org.hk/sites/default/files/hkinema01.jpg
awk -F\| 'BEGIN{printf("封面|期號|日期|下載鏈接\n---|---|---|---\n")}{printf("![%s](%s \"%s\")|[%s](%s)|%s|[PDF](%s)\n",$2,$5,$2,$2,$4,$3,$6)}' "${save_path}"
```

<details><summary>Click to expand deprecated codes</summary>

```bash
official_site='https://www.filmcritics.org.hk'
save_dir="/tmp/HKinema"
save_path="${save_dir}/HKinemaList.txt"

[[ -d "${save_dir}" ]] || mkdir -p "${save_dir}"
> "${save_path}"

download_tool='curl -fsSL'  # or 'wget -qO-'

# HKinema 創刊號 https://www.filmcritics.org.hk/taxonomy/term/7/68
list_info=${list_info:-}
list_info=$(${download_tool} https://www.filmcritics.org.hk/taxonomy/term/7/68 | sed -r -n '/src=.*?href=.*?.pdf/{s@.*?<a href="([^"]+)"[^>]*>([^<]+)<.*<a href="([^"]+)".*$@\2|'"${official_site}"'\1|'"${official_site}"'\3@g;p}' | sort -t"|" -k3,3)

# Download .pdf file via wget -qO- / curl -fsSL
echo "${list_info}" | while IFS="|" read -r name url pdf_link; do
    if [[ "${url}" =~ /node/ ]]; then
        # 第11號：電影文字緣|https://www.filmcritics.org.hk/node/776
        release_date=$(${download_tool} "${url}" | sed -r -n '/^$/d;/class="submitted"/{n;s@.*?([[:digit:]]{4}-[^[:space:]]+).*$@\1@g;p}')
    else
        # 創刊號：消失中的電影文化|https://www.filmcritics.org.hk/taxonomy/term/7/68
        # https://www.filmcritics.org.hk/taxonomy/term/7/103 has《HKinema》第十八號更正啟事, 《HKinema》第十八號
        release_date=$(${download_tool} "${url}" | sed -r -n '/^$/d;/title="《HKinema》.*號"/,/<div/{/class="submitted"/{n;s@.*?([[:digit:]]{4}-[^[:space:]]+).*$@\1@g;p}}')
    fi

    pdf_save_path="${save_dir}/${name}.${pdf_link##*.}"
    [[ -f "${pdf_save_path}" && -s "${pdf_save_path}" ]] || ${download_tool} "${pdf_link}" > "${save_dir}/${name}.${pdf_link##*.}"
    echo "${name}|${release_date}|${url}|${pdf_link}" >> "${save_path}"
    echo "${name} (${release_date}) is finished."
    sleep 1
done

# makrdown format output
# - without cover image
awk -F\| 'BEGIN{printf("期號|日期|下載鏈接\n---|---|---\n")}{printf("[%s](%s)|%s|[PDF](%s)\n",$1,$3,$2,$4)}' "${save_path}"
# - with cover image
# https://www.filmcritics.org.hk/sites/default/files/hkinema01.jpg
awk -F\| 'BEGIN{printf("封面|期號|日期|下載鏈接\n---|---|---|---\n")}{url=gensub(".*/([^.]+).pdf$","https://www.filmcritics.org.hk/sites/default/files/\\1.jpg","g",$0); printf("![%s](%s \"%s\")|[%s](%s)|%s|[PDF](%s)\n",$1,url,$1,$1,$3,$2,$4)}' "${save_path}"
```

</details>


## Hong Kong Film Archive

香港電影資料館

### Newsletter

Newsletter (通訊)

官方網址:

1. [filmarchive.gov.hk](https://www.filmarchive.gov.hk/zh_TW/web/hkfa/rp-newsletter.html)
2. [lcsd.gov.hk](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-newsletter.html)

```bash
# Date: Aug 14, 2021 Sat 14:40 ET

official_site="https://www.lcsd.gov.hk/CE/CulturalService/HKFA"

save_dir="/tmp/HKFA"
save_path="${save_dir}/Newsletter.txt"

[[ -d "${save_dir}" ]] || mkdir -p "${save_dir}"
> "${save_path}"

download_tool='curl -fsSL'  # or 'wget -qO-'

# https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-newsletter-1.html
# https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057014/newsletter01_c.pdf

nl_list_url="${official_site}/zh_TW/web/hkfa/rp-newsletter.html"
# if use 'https://www.filmarchive.gov.hk/zh_TW/web/hkfa/rp-newsletter.html', curl prompts error 'curl: (60) SSL certificate problem: unable to get local issuer certificate', solution from https://stackoverflow.com/questions/24611640/curl-60-ssl-certificate-problem-unable-to-get-local-issuer-certificate, curl add parameter '-k'

release_info_list=$(curl -fsSL "${nl_list_url}" 2> /dev/null | sed -r -n '/<article.*list-item/,/<\/article>/{s/\r$//g;s@^[[:space:]]*@@g;s@[[:space:]]*$@@g;p}' | sed ':a;N;$!ba;s@\n@@g' | sed -r -n 's@<\/article>@&\n@g;p' | sed -r -n '/<\/article>/!d;s@.*href="([^"]+)".*@\1|&@g;s@<\/h[^>]*>@&\|@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;s@[[:punct:]]?([[:digit:]]{4})@\1@g;s@[[:punct:]]*$@@g;s@^[^[:digit:]]+([[:digit:]]+).*@\1|&@g;p')
# 96|rp-newsletter-96.html|《通訊》第96期|2021年5月號

latest_rp_nl_link=$(head -n 1 <<< "${release_info_list}" | cut -d\| -f2)
# https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-newsletter-96.html
latest_rp_nl_link="${nl_list_url%/*}/${latest_rp_nl_link}"

# https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057014/newsletter[01-96]_c.pdf
pdf_link_prefix=$(curl -fsSL "${latest_rp_nl_link}" 2> /dev/null | sed -r -n 's@<\/[^>]*>@&\n@g;p' | sed -r -n '/href=.*pdf/{s@.*href=".*?(documents[^"]+)".*@'"${official_site}"'/\1@g;s@[^\/]*$@@g;p}')
# https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057014/

while IFS="|" read -r r_num r_url r_name r_date; do
    # 96|rp-newsletter-96.html|《通訊》第96期|2021年5月號
    [[ "${r_num}" -lt 10 ]] && r_num="0${r_num}"

    echo "${r_name}|${r_date}|${nl_list_url%/*}/${r_url}|${pdf_link_prefix}newsletter${r_num}_c.pdf" >> "${save_path}"
    # 《通訊》第96期|2021年5月號|https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-newsletter-96.html|https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057014/newsletter96_c.pdf
done <<< "${release_info_list}"


# - markdown format output
awk -F\| 'BEGIN{printf("Issue|Date|PDF\n---|---|---\n")}{printf("[%s](%s)|%s|[PDF](%s)\n",$1,$3,$2,$4)}' "${save_path}"

# - download pdf file
file_save_dir="${save_dir}/pdf"
[[ -d "${file_save_dir}" ]] || mkdir -p "${file_save_dir}"

cut -d\| --output-delimiter="|" -f 2,4 "${save_path}" | while IFS="|" read -r f_date f_url; do
    # 2021年5月號|https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057014/newsletter96_c.pdf
    date_format=$(sed -r -n 's@[^[:digit:]]+@-@g;s@-([[:digit:]]{1})-@0\1@g;s@-@@g;p' <<< "${f_date}")
    name_prefix=$(sed -r -n 's@.*\/([^\/]+)$@\1@g;s@_.*@@g;p;q' <<< "${f_url}")  # newsletter96
    file_save_path="${file_save_dir}/${name_prefix}-${date_format}.${f_url##*.}"  # newsletter96-202105.pdf
    [[ -s "${file_save_path}" && -f "${file_save_path}" ]] || ${download_tool} "${f_url}" > "${file_save_path}"
done
```

<details><summary>Click to expand deprecated codes</summary>

* [通訊 70~ 期](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/nletter.html)
* [通訊 (舊日通訊) 01~69期](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/nletterpast.html)

```bash
download_tool='curl -fsSL'  # or 'wget -qO-'
newsletter_dir="$HOME/newsletter"
file_save_dir="${newsletter_dir}/pdf"
list_save_path="${newsletter_dir}/list.txt"

[[ -d "${newsletter_dir}" ]] || mkdir -p "${newsletter_dir}"

nletterlist_url='https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/nletterlist.html' # > /tmp/aaa
nletterlist_url_prefix="${nletterlist_url%/*}"

> "${list_save_path}"

# https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirsnletter87.html|香港電影資料館《通訊》第87期 19年2月號
# https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirsnletterpast.html|更多舊日通訊
${download_tool} "${nletterlist_url}" | sed -r -n 's@<\/div>@&\n@g;p' | sed -r -n '/class="journal-content-article"/{s@<\/?tbody[^>]*>@&\n\n@g;p}' | sed -r -n '/通訊.*html/{s@[[:space:]]*(<[^>]*>)[[:space:]]*@\1@g;s@<\/tr>@&\n@g;p}' | sed -r -n '/href=/{s@.*href="([^"]+)"[^>]*>.*?<\/a>@'"${nletterlist_url_prefix}"'/\1|@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;p}' | while IFS="|" read -r url name;do
    pdf_url=''
    if [[ "${url}" =~ past.html$ ]]; then
        # For newsletterpast
        # http://www.archivedummy.com/b5/4-4-69.php --> https://www.lcsd.gov.hk/CE/CulturalService/HKFA/archive/b5/4-4-69.php
        newsletterpast_url=$(${download_tool} "${url}" | sed -r -n 's@<\/[^>]*>@&\n@g;p' |sed -r -n '/id="arch_pg_cntr"/{s@.*src="([^"]+)".*$@\1@g;s@.*?.com@https://www.lcsd.gov.hk/CE/CulturalService/HKFA/archive@g;p}')

        # https://www.lcsd.gov.hk/CE/CulturalService/HKFA/archive/b5/4-4-69.php|香港電影資料館第69期通訊 14年8月號
        ${download_tool} "${newsletterpast_url}" | sed -r -n '/<select[^>]*>/,/<\/select>/{/value/{s@.*value="([^"]+)"[^>]*>@'"${newsletterpast_url%/*}"'/\1|@g;s@[[:space:]]*<[^>]*>[[:space:]]*@@g;p}}' | while IFS="|" read -r past_url past_name;do

            # ../newsletters/69/69_news_c.pdf --> https://www.lcsd.gov.hk/CE/CulturalService/HKFA/archive/newsletters/69/69_news_c.pdf
            # ../newsletters/69/69_more_c.pdf 更多英譯文章
            # newsletter/newsletter60.pdf --> https://www.lcsd.gov.hk/CE/CulturalService/HKFA/archive/b5/newsletter/newsletter60.pdf
            url_str=$(${download_tool} "${past_url}" | sed -r -n '/href=.*\.pdf/{s@.*href="([^"]+)".*$@\1@g;p;q}')
            if [[ "${url_str}" =~ ^newsletter/newsletter ]]; then
                pdf_url="https://www.lcsd.gov.hk/CE/CulturalService/HKFA/archive/b5/${url_str}"
            else
                pdf_url="https://www.lcsd.gov.hk/CE/CulturalService/HKFA/archive/${url_str#*/}"
            fi
            echo "${past_name}|${past_url}|${pdf_url}" >> "${list_save_path}"
        done
    else
        # For newsletter
        pdf_url=$(${download_tool} "${url}"| sed -r -n 's@<\/div[^>]*>@&\n@g;p' | sed -r -n '/id="nletterDiv"/{s@.*href=".*?(documents[^"]+)".*@'https://www.lcsd.gov.hk/CE/CulturalService/HKFA'/\1@g;p}')
        echo "${name}|${url}|${pdf_url}" >> "${list_save_path}"
    fi
    # sleep 1
done


# - format output
sed -r -n 's@.*?(第[[:digit:]]+期)[^[:digit:]]+(.*?)號.*@\1|\2|&@;p' "${list_save_path}" | awk -F\| 'BEGIN{printf("Issue|Date|Name|PDF\n---|---|---|---\n")}{printf("%s|%s|[%s](%s)|[PDF](%s)\n",$1,$2,$3,$4,$5)}'

# - download pdf file
[[ -d "${file_save_dir}" ]] || mkdir -p "${file_save_dir}"
sed -r -n 's@.*?第([[:digit:]]+)期[^[:digit:]]+.*?\|([^\|]+)$@\1|\2@;p' "${list_save_path}" | while IFS="|" read -r f_no f_url; do
    file_path="${file_save_dir}/${f_no}.${f_url##*.}"
    [[ -s "${file_path}" && -f "${file_path}" ]] || ${download_tool} "${f_url}" > "${file_path}"
done
```

</details>

### Hong Kong Filmmakers

香港影人小傳: [中文版](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/hk-filmmakers-search.html)、[英文版](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/hk-filmmakers-search.html)。

```bash
# Date: Aug 14, 2021 Sat 19:30 ET

official_site='https://www.lcsd.gov.hk/CE/CulturalService/HKFA/'
download_tool='curl -fsSL'  # or 'wget -qO-'

save_dir='/tmp/HKFA'
[[ -d "${save_dir}" ]] || mkdir -p "${save_dir}"
zh_info="${save_dir}/zh"
en_info="${save_dir}/en"
save_path="${save_dir}/HongKongFilmmakers.txt"

# Download html code via wget -qO- / curl -fsSL
# zh_TW
${download_tool} 'https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/hk-filmmakers-search.html' | sed -r 's/\r$//g;s@<\/[^>]*>@&\n@g;' | sed -r -n '/\.pdf/{s@.*href=".*?/(documents[^"]+)"[^>]*>[[:space:]]*([^<]+)<.*$@'"${official_site}"'\1|\2@g;p}' | sed -r -n 's@^.*[[:digit:]]+/([^_]+)_.*?.pdf@\1|&@g;p' > "${zh_info}"
# en_US
${download_tool} 'https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/hk-filmmakers-search.html' | sed -r 's/\r$//g;s@<\/[^>]*>@&\n@g;' | sed -r -n '/\.pdf/{s@<span[^>]*>@@g;s@.*href=".*?/(documents[^"]+)"[^>]*>[[:space:]]*([^<]+)<.*$@'"${official_site}"'\1|\2@g;s@[[:space:]]+@%2b@g;p}' | sed -r -n 's@^.*[[:digit:]]+/([^_]+)_.*?.pdf@\1|&@g;p' > "${en_info}"


# - Merge multiple files into one file via awk
awk -F\| 'NR==FNR{a[$1]=$0} NR>FNR {print a[$1]"|"$0}' "${zh_info}" "${en_info}" | awk -F\| 'BEGIN{OFS="|"}{gsub(/%2b/," ",$4);print $3,$4,$2,$5}' | sort -t"|" -k2,2 > "${save_path}"

[[ -d "${temp_save_dir}" ]] && rm -rf "${temp_save_dir}"

# makrdown format output
awk -F\| 'BEGIN{printf("中文名|英文名|PDF\n---|---|---\n")}{printf("%s|%s|[zh](%s)/[en](%s)\n",$1,$2,$3,$4)}' "${save_path}" 
```


<details><summary>Click to expand deprecated codes</summary>

```bash
official_site='https://www.lcsd.gov.hk/CE/CulturalService/HKFA/'
download_tool='curl -fsSL'  # or 'wget -qO-'

temp_save_dir=$(mktemp -d XXXXXXXXX)
zh_info="${temp_save_dir}/zh"
en_info="${temp_save_dir}/en"
save_path="$HOME/HongKongFilmmakers.txt"


# Download html code via wget -qO- / curl -fsSL
# zh_TW
${download_tool} https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/hk_filmmaker_search.html | sed -r 's@<\/[^>]*>@&\n@g;' | sed -r -n '/\.pdf/{s@.*href=".*?/(documents[^"]+)"[^>]*>[[:space:]]*([^<]+)<.*$@'"${official_site}"'\1|\2@g;p}' | sed -r -n 's@^.*[[:digit:]]+/([^_]+)_.*?.pdf@\1|&@g;p' > "${zh_info}"
# en_US
${download_tool} https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/hk_filmmaker_search.html | sed -r 's@<\/[^>]*>@&\n@g;' | sed -r -n '/\.pdf/{s@.*href=".*?/(documents[^"]+)"[^>]*>[[:space:]]*([^<]+)<.*$@'"${official_site}"'\1|\2@g;s@[[:space:]]+@%2b@g;p}' | sed -r -n 's@^.*[[:digit:]]+/([^_]+)_.*?.pdf@\1|&@g;p' > "${en_info}"

# awk merge multiple files into one file
awk -F\| 'NR==FNR{a[$1]=$0} NR>FNR {print a[$1]"|"$0}' "${zh_info}" "${en_info}" | awk -F\| 'BEGIN{OFS="|"}{gsub(/%2b/," ",$4);print $3,$4,$2,$5}' | sort -t"|" -k2,2 > "${save_path}"

[[ -d "${temp_save_dir}" ]] && rm -rf "${temp_save_dir}"

# makrdown format output
awk -F\| 'BEGIN{printf("中文名|英文名|下載鏈接\n---|---|---\n")}{printf("%s|%s|[zh](%s)|[en](%s)\n",$1,$2,$3,$4)}' "${save_path}"
```

</details>

### Hong Kong Film Search

通過 香港電影資料館 提供的檢索工具 [香港電影檢索 (進階)](https://mcms.lcsd.gov.hk/Search/hkFilm/enquireAdvancedSearch!) 進行數據查找。

<!-- 基本 https://mcms.lcsd.gov.hk/Search/hkFilm/enquire! -->
<!-- [香港電影檢索 (進階)](https://ipac.hkfa.lcsd.gov.hk/ipac/AS/AS-002-02_Tc.zul) -->

```bash
# 搜索關鍵詞：邵氏，注意`--data-raw`中的字段`form.*`

# From Web Browser Developer Tool

# curl $'https://mcms.lcsd.gov.hk/Search/hkFilm/enquireAdvancedSearch\u0021searchRecords' -H 'Connection: keep-alive' -H 'Cache-Control: max-age=0' -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="92"' -H 'sec-ch-ua-mobile: ?0' -H 'Origin: https://mcms.lcsd.gov.hk' -H 'Upgrade-Insecure-Requests: 1' -H 'DNT: 1' -H 'Content-Type: application/x-www-form-urlencoded' -H 'User-Agent: Mozilla/5.0 (Windows NT 11.0; Win64; x64; rv:99.0) Gecko/20100101 Firefox/99.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' -H 'Sec-Fetch-Site: same-origin' -H 'Sec-Fetch-Mode: navigate' -H 'Sec-Fetch-User: ?1' -H 'Sec-Fetch-Dest: document' -H $'Referer: https://mcms.lcsd.gov.hk/Search/hkFilm/enquireAdvancedSearch\u0021' -H 'Accept-Language: en-US,en;q=0.9' -H 'Cookie: Secure; JSESSIONID=5rKoxy82F9AtM6xFY4Ad0qrtjnS6vdNocc0oZbDh.slva03pmcms'  --data-raw 'form.formAction=&form.mode=default&form.editRecord=&form.lang_code=TC&form.film_title=&form.film_lang=&form.release_year_from=&form.release_year_to=&form.personal_names_relation=AND&form.personal_name_1=&form.personal_name_2=&form.personal_name_3=&form.group_name=%E9%82%B5%E6%B0%8F&form.hasChange=true&form.dialogHasChange=true&form.back=false&maintainSubmit=true'  --compressed

# for 2nd page, see 'form.result.page=2' in '--data-raw'
# --data-raw 'form.formAction=changePage&form.mode=default&form.editRecord=&form.lang_code=TC&form.film_title=&form.film_lang=&form.release_year_from=&form.release_year_to=&form.personal_names_relation=AND&form.personal_name_1=&form.personal_name_2=&form.personal_name_3=&form.group_name=%E9%82%B5%E6%B0%8F&form.result.page=2&form.result.--data-raw=1&form.result.sortingColumn=film_title&form.result_pageNoTop=&form.result_pageNoBottom=&form.hasChange=&form.dialogHasChange=&form.back=false&maintainSubmit=false'

```

<details><summary>Click to expand deprecated codes</summary>

```bash
# 搜索關鍵詞：邵氏，注意`--data`中的字段`data_0`

# From Web Browser Developer Tool
# curl 'https://ipac.hkfa.lcsd.gov.hk/ipac/zkau' -H 'Origin: https://ipac.hkfa.lcsd.gov.hk' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: en-US,en;q=0.9,zh-TW;q=0.8,zh;q=0.7' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3690.9d Safari/537.36 OPR/46.0.2202.95' -H 'Content-Type: application/x-www-form-urlencoded;charset=UTF-8' -H 'Accept: */*' -H 'Referer: https://ipac.hkfa.lcsd.gov.hk/ipac/AS/AS-002-02_Tc.zul' -H 'Cookie: JSESSIONID=0001ZV0He8UXFTAmCwmpDsGxUwA:17erij8fe' -H 'Connection: keep-alive' -H 'DNT: 1' -H 'ZK-SID: 3591' --data 'dtid=z__&cmd_0=onClick&uuid_0=gI4Cac&data_0=%7B%22pageX%22%3A185%2C%22pageY%22%3A342%2C%22which%22%3A1%2C%22x%22%3A44%2C%22y%22%3A22%7D' --compressed

cookie_string='0001ZV0He8UXFTAmCwmpDsGxUwA:17erij8fe'
zk_sid='3591'
# https://www.zkoss.org/wiki/ZK_Developer's_Reference/Security_Tips/Denial_Of_Service#Prevent_sending_same_request_multiple_times
save_path="$HOME/HKFS.txt"
> "${save_path}"

user_agent='Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3690.9d Safari/537.36 OPR/46.0.2202.95'

first_page_html=$(${download_tool} 'https://ipac.hkfa.lcsd.gov.hk/ipac/zkau' -H 'Origin: https://ipac.hkfa.lcsd.gov.hk' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: en-US,en;q=0.9,zh-TW;q=0.8,zh;q=0.7' -H 'User-Agent: '"${user_agent}"'' -H 'Content-Type: application/x-www-form-urlencoded;charset=UTF-8' -H 'Accept: */*' -H 'Referer: https://ipac.hkfa.lcsd.gov.hk/ipac/AS/AS-002-02_Tc.zul' -H 'Cookie: JSESSIONID='"${cookie_string}"'' -H 'Connection: keep-alive' -H 'DNT: 1' -H 'ZK-SID: '"${zk_sid}"'' --data 'dtid=z__&cmd_0=onClick&uuid_0=gI4Cac&data_0=%7B%22pageX%22%3A169%2C%22pageY%22%3A328%2C%22which%22%3A1%2C%22x%22%3A28%2C%22y%22%3A8%7D' --compressed)

page_num=$(echo "${first_page_html}" | sed -r -n '/pageCount/{s@'\"'@@g;s@.*pageCount,([^]]+).*$@\1@g;p}')

if [[ -n "${page_num}" && "${page_num}" -gt 0 ]]; then
    # page 1
    echo "${first_page_html}" | sed -r -n '/(label|value)/!d;s@'"'"'@@g;/label:/{s@.*label:([^}]+).*$@---\1@g;};/value:/{s@.*value:([^}]+).*$@\1@g;};p' | sed ':a;N;$!ba;s@\n@|@g' | sed -r -n 's@^-+@@g;s@\|?---@\n@g;p' > "${save_path}"
    echo 'Page 1 is ok.'

    # page>1
    for (( i = 1; i < ${page_num}; i++ )); do
        current_page=$(($i+1))
        ((zk_sid++))
        page_html=$(${download_tool} 'https://ipac.hkfa.lcsd.gov.hk/ipac/zkau' -H 'Origin: https://ipac.hkfa.lcsd.gov.hk' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: en-US,en;q=0.9,zh-TW;q=0.8,zh;q=0.7' -H 'User-Agent: '"${user_agent}"'' -H 'Content-Type: application/x-www-form-urlencoded;charset=UTF-8' -H 'Accept: */*' -H 'Referer: https://ipac.hkfa.lcsd.gov.hk/ipac/AS/AS-002-02_Tc.zul' -H 'Cookie: JSESSIONID='"${cookie_string}"'' -H 'Connection: keep-alive' -H 'DNT: 1' -H 'ZK-SID: '"${zk_sid}"'' --data 'dtid=z__&cmd_0=onPaging&uuid_0=gI4Cdc&data_0=%7B%22%22%3A'"${i}"'%7D' --compressed)
        list_info=$(echo "${page_html}" | sed -r -n '/(label|value)/!d;s@'"'"'@@g;/label:/{s@.*label:([^}]+).*$@---\1@g;};/value:/{s@.*value:([^}]+).*$@\1@g;};p' | sed ':a;N;$!ba;s@\n@|@g' | sed -r -n 's@^-+@@g;s@\|?---@\n@g;p')
        if [[ -n "${list_info}" ]]; then
            echo "${list_info}" >> "${save_path}"
            echo "Page ${current_page} is ok."
            sleep 1
        else
            echo "Fail to extract page ${current_page}."
            break
        fi
    done
else
    echo 'Fail to extract first page html.'
fi
```

</details>


## Change Log

* Aug 14, 2021 20:10 Sat ET
  * Shell腳本更新

<!-- End -->