# Oral History Series

香港影人口述歷史叢書 : [中文版](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-oral-history-series.html)、[英文版](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-oral-history-series.html)。

<!-- [中文版](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/oralhistoryseries.html)、[英文版](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/oralhistoryseries.html) -->

本館進行的「香港影人口述歷史計劃」，廣泛訪問影壇前輩，訪問過程用數碼錄像及錄音記錄下來，成為載聲載影的珍貴活歷史。我們從而進行專題探討，陸續將數據整理成書。書中並載有珍貴照片，及附有各人的作品年表。

> Our Oral History Project is a documentation of interviews with Hong Kong film veterans by means of digital and A/V technology, which serves as the basis for further researches and thematic publications. The 'Oral History Series' contains also precious photos and filmographies of individual filmmakers.

## Book Lists

系列|香港影人口述歷史叢書|Oral History Series|出版日期|說明
---|---|---|---|---
1|[南來香港](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-oral-history-series-1.html)|[Hong Kong Here I Come](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-oral-history-series-1.html)|2000|全書226頁，中英對照，載有百多張珍貴照片，並附有各人的作品年表。
2|[理想年代──長城、鳳凰的日子](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-oral-history-series-2.html)|[An Age of Idealism: Great Wall & Feng Huang Days](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-oral-history-series-2.html)|2001|全書416頁，中英對照，載有百多幀珍貴照片，並附各人的作品年表。
3|[楚原](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-oral-history-series-3.html)|[Director Chor Yuen](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-oral-history-series-4.html)|2006|全書120頁
4|[王天林](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-oral-history-series-5.html)|[Director Wong Tin-lam](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-oral-history-series-5.html)|2007|全書264頁
5|[摩登色彩──邁進1960年代](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-oral-history-series-6.html)|[An Emerging Modernity: Hong Kong Cinema of the 1960s](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-oral-history-series-6.html)|2008|全書356頁
6|[龍剛](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-oral-history-series-7.html)|[Director Lung Kong](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-oral-history-series-7.html)|2010|全書280頁（附英文翻譯光碟）
7|[風起潮湧──七十年代香港電影](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-oral-history-series-8.html) ([PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057030/Oral%2bHistory%2bSeries_chi07_ebook.pdf))|[When the Wind Was Blowing Wild: Hong Kong Cinema of the 1970s](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-oral-history-series-9.html) ([PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057029/Oral%2bHistory%2bSeries_eng07_ebook.pdf))|2018|全書254頁


### Deprecated

<details><summary>Click to expand deprecated note</summary>

系列|圖片|香港影人口述歷史叢書|Oral History Series|出版日期|說明
---|---|---|---|---|---
1|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-f772cf1b-40a2-4654-bbf8-f8b5f3ec82a1-2.html)|[南來香港](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail01.html)|[Hong Kong Here I Come](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail01.html)|2000|全書226頁，中英對照，載有百多張珍貴照片，並附有各人的作品年表。
2|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-abb8469a-078c-4811-98c7-45f7ca47b4f2-2.html)|[理想年代──長城、鳳凰的日子](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail02.html)|[An Age of Idealism: Great Wall & Feng Huang Days](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail02.html)|2001|全書416頁，中英對照，載有百多幀珍貴照片，並附各人的作品年表。
3|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-b6eca8b5-d17d-4a93-950e-e6f4042d5a40-2.html)|[楚原](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail03.html)|[Director Chor Yuen](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail03.html)|2006|全書120頁
4|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-53098dfa-40f2-4b50-9356-f55334a8f6bd-2.html)|[王天林](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail04.html)|[Director Wong Tin-lam](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail04.html)|2007|全書264頁
5|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-380bd194-2360-4ea0-b9ae-07bf720eb3b0-2.html)|[摩登色彩──邁進1960年代](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail05.html)|[An Emerging Modernity: Hong Kong Cinema of the 1960s](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail05.html)|2008|全書356頁
6|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-a4c9fe22-fa6d-45ed-bc56-8aa3ddcfe36a.html) |[龍剛](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail06.html)|[Director Lung Kong](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail06.html)|2010|全書280頁（附英文翻譯光碟）
7|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-49a02c48-137c-41f2-8e56-1f0c3b8d7a48-3.html)|[風起潮湧──七十年代香港電影](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail07.html) ([PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/7439981/ebook_chi_01.pdf))|[When the Wind Was Blowing Wild: Hong Kong Cinema of the 1970s](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/oralhistoryseries/oralhistoryseries_detail07.html) ([PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/7439981/ebook_eng_01.pdf))|2018|全書254頁

</details>


## Chnage Log

* Feb 23, 2019 16:36 Sat ET
  * 初稿完成
* Aug 14, 2021 18:45 Sat ET
  * 鏈接更新

<!-- End -->
