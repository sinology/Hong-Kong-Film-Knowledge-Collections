# Hong Kong Filmography Series

香港影片大全系列 : [中文版](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-hk-filmography-series.html)、[英文版](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-hk-filmography-series.html)

<!-- [中文版](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/hkfilmographyseries.html)、[英文版](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/hkfilmographyseries.html) -->

「香港影片大全系列」務求整理出百多年來香港影片的輪廓。每卷按年代羅列香港出品的故事片及紀錄片的資料，包括影片的類型、語別、公映日期、出品公司、主要工作人員名單、編／導／演的話、故事大綱、附註及劇照等，並載有編年表及索引，方便讀者以片名、影人或電影公司索查資料。精裝印製，第一至八卷現已出版，包括1914至1979年所攝製的共七千多部影片，資料彌足珍貴。

>The Hong Kong Filmography Series is an attempt to chronicle a hundred years of Hong Kong cinema and history in a series of publications providing key information such as the genre, language, production companies, cast and crew, directors' notes, illustrated by precious film stills. A film list and indices are also appended. Eight volumes of the Filmographies have already been released, featuring over 7,000 fiction films and documentaries produced between 1914 and 1979.

## Book Lists

系列|香港影片大全系列|Hong Kong Filmography Series|出版日期|說明
---|---|---|---|---
Vol I|[第一卷增訂本（1914–1941）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-hk-filmography-series-10.html)|[Vol I (1914–1941) (Revised Edition)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-hk-filmography-series-10.html)|2020|增訂本以1997年初版為基礎，參照本館搜集所得的新資料加以訂正及增補。羅列1914至1941年間香港出品的六百多部故事片及紀錄片的資料。中文版全書352頁，24頁精美特刊封面彩頁，附有編年表及索引。
Vol I|[第一卷（1913-1941）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-hk-filmography-series-1.html)|[Volume I (1913-1941)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-hk-filmography-series-1.html)|1997|羅列1913至1941年間香港出品的六百多部故事片及紀錄片的資料。全書696頁，中英對照。
Vol II|[第二卷（1942-1949）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-hk-filmography-series-2.html)|[Vol II (1942–1949)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-hk-filmography-series-2.html)|1998|羅列1942至1949年間香港出品的四百多部故事片及紀錄片的資料。全書559頁，中英對照。
Vol III|[第三卷（1950-1952）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-hk-filmography-series-3.html)|[Vol III (1950–1952)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-hk-filmography-series-3.html)|2000|第三卷羅列1950至1952年間香港出品的五百八十多部故事片及紀錄片的資料。全書761頁，中英對照。
Vol IV|[第四卷（1953-1959）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-hk-filmography-series-4.html)|[Vol IV (1953–1959)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-hk-filmography-series-5.html)|2003|第四卷羅列1953至1959年間香港出品的一千六百多部故事片及紀錄片的資料。分中、英文兩冊。中文版540頁，英文版696頁。
Vol V|[第五卷（1960-1964）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-hk-filmography-series-6.html)|[Vol V (1960–1964)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-hk-filmography-series-6.html)|2005|羅列1960至1964年間香港出品的一千二百多部故事片及紀錄片的資料。全書560頁，24頁精美海報彩頁。
Vol VI|[第六卷（1965-1969）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-hk-filmography-series-7.html)|[Vol VI (1965–1969)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-hk-filmography-series-7.html)|2007|羅列1965至1969年間香港出品的九百多部故事片及紀錄片的資料。全書452頁，16頁精美海報彩頁。
Vol VII|[第七卷（1970-1974）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-hk-filmography-series-8.html)|[Vol VII (1970–1974)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-hk-filmography-series-8.html)|2010|羅列1970至1974年間香港出品的九百多部故事片及紀錄片的資料。全書464頁，16頁精美海報彩頁。
Vol VIII|[第八卷（1975-1979）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-hk-filmography-series-9.html)|[Vol VIII (1975–1979)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/rp-hk-filmography-series-9.html)|2014|羅列1975至1979年間香港出品的八百多部故事片及紀錄片的資料。全書524頁，16頁精美海報彩頁。


### Deprecated

<details><summary>Click to expand deprecated note</summary>

系列|圖片|香港影片大全系列|Hong Kong Filmography Series|出版日期|說明
---|---|---|---|---|---
Vol I|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-cae420c9-3ae9-45ac-8af2-3ec450d3c67d.html)|[第一卷（1913-1941）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail01.html)|[Volume I (1913-1941)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail01.html)|1997|羅列1913至1941年間香港出品的六百多部故事片及紀錄片的資料。全書696頁，中英對照。
Vol II|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-ee4de8da-bc86-45e3-9058-5f131836a017.html)|[第二卷（1942-1949）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail02.html)|[Vol II (1942–1949)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail02.html)|1998|羅列1942至1949年間香港出品的四百多部故事片及紀錄片的資料。全書559頁，中英對照。
Vol III|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-d66c2007-7149-4f15-93c2-106db0d874dc.html)|[第三卷（1950-1952）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail03.html)|[Vol III (1950–1952)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail03.html)|2000|第三卷羅列1950至1952年間香港出品的五百八十多部故事片及紀錄片的資料。全書761頁，中英對照。
Vol IV|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-1e9e9446-e784-4646-a4a4-b952ccf34baa.html)|[第四卷（1953-1959）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail04.html)|[Vol IV (1953–1959)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail04.html)|2003|第四卷羅列1953至1959年間香港出品的一千六百多部故事片及紀錄片的資料。分中、英文兩冊。中文版540頁，英文版696頁。
Vol V|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-a11db046-a292-4516-88ac-96d7b0e90893.html)|[第五卷（1960-1964）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail05.html)|[Vol V (1960–1964)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail05.html)|2005|羅列1960至1964年間香港出品的一千二百多部故事片及紀錄片的資料。全書560頁，24頁精美海報彩頁。
Vol VI|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-ddc5e996-433f-4c7d-8f22-15e20da3cc30.html)|[第六卷（1965-1969）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail06.html)|[Vol VI (1965–1969)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail06.html)|2007|羅列1965至1969年間香港出品的九百多部故事片及紀錄片的資料。全書452頁，16頁精美海報彩頁。
Vol VII|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-2bbf226f-26e4-4d8e-9f8d-ee0cce33e729.html)|[第七卷（1970-1974）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail07.html)|[Vol VII (1970–1974)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail07.html)|2010|羅列1970至1974年間香港出品的九百多部故事片及紀錄片的資料。全書464頁，16頁精美海報彩頁。
Vol VIII|![](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/image/image_gallery-9171e00a-16f2-49ec-a8c5-7acf5aaddfba-2.html)|[第八卷（1975-1979）](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/hkfilmographyseries/hkfilmographyseries_detail08.html)|[Vol VIII (1975–1979)](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/en_US/web/hkfa/publications_souvenirs/pub/hkfilmographyseries.html)|2014|羅列1975至1979年間香港出品的八百多部故事片及紀錄片的資料。全書524頁，16頁精美海報彩頁。

</details>


## Chnage Log

* Feb 23, 2019 16:36 Sat ET
  * 初稿完成
* Aug 14, 2021 19:45 Sat ET
  * 鏈接更新

<!-- End -->
