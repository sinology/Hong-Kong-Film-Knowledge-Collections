# Hong Kong Film Archive - E-publications

E-publications (電子刊物)

官方網址:

1. [filmarchive.gov.hk](https://www.filmarchive.gov.hk/zh_TW/web/hkfa/rp-electronic-publications-list.html)
2. [lcsd.gov.hk](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-electronic-publications-list.html)


## Book Lists

Category|Title|PDF|Exist
---|---|---|---
港產電影一覽|港產電影一覽（1914-2010）20111028|local [PDF](./books/港產電影一覽(1913-2003)_20060315.pdf)|N
港產電影一覽|港產電影一覽(1913-2010) 20110715|local [PDF](./books/港產電影一覽(1913-2010)_20110715.pdf)|N
港產電影一覽|港產電影一覽(1913-2003) 20060315|local [PDF](./books/港產電影一覽(1913-2003)_20060315.pdf)|N
香港早期聲影|[五、六十年代香港粵語戲曲電影片目](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/resources.html)|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057013/7-2-2.pdf)|N
香港早期聲影|尋存與啟迪-香港早期聲影遺珍|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057015/EC_ebook_TC.pdf)|
香港早期電影遊蹤|香港早期電影遊蹤-節目特刊第1冊(早期香港影像)|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057015/ebrochure_01.pdf)|
香港早期電影遊蹤|香港早期電影遊蹤-節目特刊第2冊(電影先驅侯曜)|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057015/ebrochure_02.pdf)|
香港早期電影遊蹤|香港早期電影遊蹤-節目特刊第3冊(被遺忘的影壇女先鋒及大觀公司的越洋製作)|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057015/ebrochure_03.pdf)|
香港影人口述歷史|香港影人口述歷史叢書之七：風起潮湧──七十年代香港電影（中文版）：風起潮湧──七十年代香港電影（中文版）|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057015/ebook_chi_01.pdf)|
香港影人口述歷史|香港影人口述歷史叢書之七：風起潮湧──七十年代香港電影（英文版）|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057015/ebook_eng_01.pdf)|
香港影人口述歷史|【編+導】回顧系列四：李萍倩|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057015/ehouseprog_03_TC.pdf)|
香港影人口述歷史|小城內外的韋偉|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057015/ehouseprog_01_TC.pdf)|
香港影人口述歷史|芳華年代|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057015/ehouseprog_02_TC.pdf)|
x|香港電影資料館十周年紀念特刊|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057015/hkfa_10th.pdf)|
x|[香港電影資料館二十周年號外](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/pe.html#proFolio)|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/20963254/Hong%2bKong%2bFilm%2bArchive%2bSpecial%2bVicennial%2bEditionchi.pdf)|New
x|[百部不可不看的香港電影](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/pe.html#must-see)|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057011/100_Must_See_Booklet.pdf)|
x|光影中的虎度門──香港粵劇電影研究（中文版）|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057015/ebook_chi_02.pdf)|New
x|光影中的虎度門──香港粵劇電影研究（英文版）|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/18995340/19057015/ebook_eng_02.pdf)|New


### Deprecated

<details><summary>Click to expand deprecated list</summary>

Category|Title|PDF URL
---|---|---
港產電影一覽|港產電影一覽（1914-2010）20111028|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/2007315/7-2-1.pdf)
港產電影一覽|港產電影一覽(1913-2010) 20110715|
港產電影一覽|港產電影一覽(1913-2003) 20060315|
x|香港電影資料館十周年紀念特刊|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/2007294/hkfa_10th.pdf)
x|百部不可不看的香港電影|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/2007347/100_booklet.pdf)
香港早期聲影|五、六十年代香港粵語戲曲電影片目|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/2007342/7-2-2.pdf)
香港早期聲影|尋存與啟迪-香港早期聲影遺珍|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/2007294/EC_ebook_TC.pdf)
香港早期電影遊蹤|香港早期電影遊蹤-節目特刊第1冊(早期香港影像)|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/2007294/ebrochure_01.pdf)
香港早期電影遊蹤|香港早期電影遊蹤-節目特刊第2冊(電影先驅侯曜)|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/2007294/ebrochure_02.pdf)
香港早期電影遊蹤|香港早期電影遊蹤-節目特刊第3冊(被遺忘的影壇女先鋒及大觀公司的越洋製作)|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/2007294/ebrochure_03.pdf)
香港影人口述歷史|香港影人口述歷史叢書之七：風起潮湧──七十年代香港電影（中文版）|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/7439981/ebook_chi_01.pdf)
香港影人口述歷史|香港影人口述歷史叢書之七：風起潮湧──七十年代香港電影（英文版）|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/7439981/ebook_eng_01.pdf)
香港影人口述歷史|【編+導】回顧系列四：李萍倩|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/7439982/ehouseprog_03_TC.pdf)
香港影人口述歷史|小城內外的韋偉|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/7439982/ehouseprog_01_TC.pdf)
香港影人口述歷史|芳華年代|[PDF](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/documents/2005525/7439982/ehouseprog_02_TC.pdf)

</details>


## Chnage Log

* Aug 14, 2021 20:55 Sat ET
  * 初稿，鏈接更新

<!-- End -->
