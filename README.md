# 香港電影資料彙集

此項目彙總關於香港電影的資料，大部分是本人於2009~2013年間收集，資料來源有光影久久、時代影視論壇、Norm影視論壇、榕城客影視俱樂部等，有些論壇早已無法訪問。現將其重新整理，以緬懷那逝去的青春。

大部分資料都是通過搜索引擎及官方網站獲取，部分資料可能涉及到版權問題，請勿作商用。

博客 [香港文化資料庫][hongkongcultures] 刊有與香港相關的資料，[香港電臺網站](https://www.rthk.hk) 提供 [Podcasts](https://podcast.rthk.hk/podcast/) 服務，內容涵蓋新聞、時事、資訊、文化、音樂、娛樂等，對香港文化有興趣者可移步一觀。


## TOC

1. [Hong Kong Memory](#hong-kong-memory)  
2. [Definition Of Hong Kong Film](#definition-of-hong-kong-film)  
3. [Hong Kong Motion Picture Industry Association](#hong-kong-motion-picture-industry-association)  
4. [Create Hong Kong](#create-hong-kong)  
4.1 [HK Film Industry Info](#hk-film-industry-info)  
5. [Hong Kong File Critics Society](#hong-kong-file-critics-society)  
6. [Hong Kong Film Archive](#hong-kong-film-archive)  
6.1 [Film Maker](#film-maker)  
6.2 [E-publications](#e-publications)  
6.3 [Newsletter](#newsletter)  
7. [Shaw Brothers Film](#shaw-brothers-film)  
7.1 [Film List](#film-list)  
7.2 [Genres](#genres)  
8. [Filmology](#filmology)  
9. [Miscellaneous](#miscellaneous)  
10. [Chnage Log](#chnage-log)


## Hong Kong Memory

[香港公共圖書館](https://www.hkpl.gov.hk 'Hong Kong Public Libraries')於2017年3月編印了『[香港記憶：歷史文化館中尋](http://www.hkpl.gov.hk/en/common/attachments/reference/resources/HK%20Memory20170405.pdf 'Hong Kong Memory : Hong Kong History and Culture at Libraries')』一書，詳細介紹了有關於香港歷史、文化的資料及獲取途徑。

『[香港記憶][hkmemory]』的成立是為了響應聯合國教科文組織推動的「[世界記憶][mow]」計劃。「[世界記憶][mow]」旨在鼓勵各地將歷史檔案和圖書館珍貴館藏以數碼形式保存,讓全世界分享這些集體回憶。

為此,『[香港記憶][hkmemory]』網站 (<https://www.hkmemory.hk>) 作為一個多媒體數碼平台,免費提供香港的歷史和文化資料,包括文獻、圖片、海報、錄音、電影及錄像。目的是以數碼形式保存香港的歷史及文化遺產,將散落的歷史及文化資料集中儲藏於一個數碼檔案庫及提供方便的平台,讓全球讀者免費瀏覽這些資料。

[香港記憶][hkmemory]的專題特藏中有關於[邵氏電影][hkmemory_shaw_brothers_movies]的文本、聲音與視聽資料。


## Definition Of Hong Kong Film

[香港影業協會][hk_mpia]在其官網給出了[香港電影定義](http://www.mpia.org.hk/content/about_definition.php)，[創意香港][hk_create]出版的刊物《香港電影業資料彙編》也對香港電影給出定義，二者基本一致。

>定義 **甲**: 所有出品公司均是香港註冊公司,有關影片即屬香港電影;或 (如非所有出品公司均是香港註冊公司,須參看乙項要求。)
>
>定義 **乙**: 影片必須同時符合下列兩項要求:
>
>1. 出品公司中至少有一間為香港註冊公司;及
>2. 以下5個有效崗位之中，50%或以上的崗位由香港永久居民擔任:監製(或出品人)、導演、編劇、主要男演員及主要女演員。所謂有效崗位即有人擔任的崗位,例如一部講述動物的紀錄片中,主要男女演員並非有效崗位,如果同時沒有編劇的話,50%或以上的崗位即導演或監製(或出品人)其中一方是香港永久居民即可。一般5個崗位均為有效的情況下,須有3個崗位由香港永久居民擔任。
>
>此外,有關影片長度必須是60分鐘或以上,而影片須在香港戲院正式上映(開畫日子以正式上映日期為準,不計算
午夜場或優先場),並作公開售票。


1. 「港產片」：符合甲項定義的香港電影；
2. 「合拍片」：符合乙項定義要求的香港電影；
3. 「華語片」 (或華語電影)：以華語(即普通話或國語)製作的電影,廣義來說,於香港、中國內地、台灣、澳門、新加坡,以及馬來西亞等地出產,而以華語或中國地方語言製作的電影都稱為華語片；
4. 「外語片」：以非華語或非中國地方語言製作而香港並無參與的電影；


## Hong Kong Motion Picture Industry Association

[香港影業協會][hk_mpia]成立於一九八六年七月十六日，旨在：

1. 推廣及保障業內人士權益；積極打擊任何盜版侵權的活動
2. 代表電影行業及有關人士，就有關電影業的事件發表意見，及對有關意見作出回應
3. 代表電影行業及有關人士，與香港政府或其他國家的政府進行磋商
4. 與世界各地的影業協會聯絡，以促進、推廣及保障電影業的發展
5. 代表協會會員或業內其他人士對侵犯版權或其他權利者進行法律訴訟。

網站提供歷年及當年香港電影票房統計數據，需會員登入才能訪問。但可通過[搜索引擎](https://www.google.com/search?q=site%3Ahttp%3A%2F%2Fwww.mpia.org.hk+filetype%3Apdf)可搜索到，如[2019農曆新年賀歲檔期電影票房簡報](http://www.mpia.org.hk/upload/file/00260/2019CNYBO.pdf) (Feb 8, 2019)、[2018年度十大票房](www.mpia.org.hk/upload/file/00259/2018年度十大票房.pdf) (Dec 31, 2018)。

其[官網](http://mpia.org.hk/content/links.php)提供了一些與香港電影香港的機構 ([script](./Script/README.md#hong-kong-motion-picture-industry-association))

* [創意香港](http://www.createhk.gov.hk/ "Create Hong Kong") (Create Hong Kong)
* [香港知識產權署](http://www.ipd.gov.hk/ "Intellectual  Property Department") (Intellectual  Property Department)
* [香港海關](http://www.customs.gov.hk/ "Customs  and Excise Department") (Customs  and Excise Department)
* [保護知識產權大聯盟](http://www.iprpa.org/trad/front.php "Intellectual  Property Rights Protection Alliance") (Intellectual  Property Rights Protection Alliance)
* [香港電影發展局](http://www.fdc.gov.hk/tc/home/index.htm "Hong  Kong Film Development Council") (Hong  Kong Film Development Council)
* [香港電影資料館](http://www.lcsd.gov.hk/CE/CulturalService/HKFA/index.php "Hong Kong Film Archive") (Hong Kong Film Archive)
* [亞洲電影大獎](http://www.asianfilmawards.asia/ "Asian  Film Awards") (Asian  Film Awards)
* [香港電影金像獎](http://www.hkfaa.com/ "Hong  Kong Film Awards") (Hong  Kong Film Awards)
* [香港電影工作者總會](http://www.hkfilmmakers.com/ "Federation  of Hong Kong Filmmakers") (Federation  of Hong Kong Filmmakers)
* [香港電影導演會](http://www.hkfdg.com/ "Hong  Kong Film Directors' Guild") (Hong  Kong Film Directors' Guild)
* [香港電影編劇家協會](http://www.hkswg.com/ "Hong  Kong Screenwriters' Guild") (Hong  Kong Screenwriters' Guild)
* [香港演藝人協會](http://hkpag.org/hkpag/ "Hong Kong Performing Artistes Guild") (Hong Kong Performing Artistes Guild)
* [香港電影評論學會](http://filmcritics.org.hk/ "Hong Kong Film Critics Society") (Hong Kong Film Critics Society)


## Create Hong Kong

[創意香港][hk_create]是香港特別行政區政府商務及經濟發展局其下一個專責辦公室，於二零零九年六月一日成立，重點工作是去牽頭、倡導和推動本港創意經濟的發展。創意香港統籌政府在創意產業方面的政策和工作；把政府用作推動和加快香港創意產業發展的資源集中起來；並與業界緊密合作，推動創意產業的發展。

[創意香港][hk_create]官網列出了 [電影拍攝指南](http://www.fso-createhk.gov.hk/tc/guidetofilming.php) (2021年4月編制)

目錄|FSO/PDF
---|---
第01章 電影服務統籌科|[FSO](https://www.fso-createhk.gov.hk/flipbook.php?lang=Chi&f=01)/[PDF](https://www.fso-createhk.gov.hk/pdf/Chap01-Chi01.pdf)
第02章 香港電影業|[FSO](https://www.fso-createhk.gov.hk/flipbook.php?lang=Chi&f=02)/[PDF](https://www.fso-createhk.gov.hk/pdf/Chap02-Chi02.pdf)
第03章 外景拍攝|[FSO](https://www.fso-createhk.gov.hk/flipbook.php?lang=Chi&f=03)/[PDF](https://www.fso-createhk.gov.hk/pdf/Chap03-Chi03.pdf)
第04章 政府規例|[FSO](https://www.fso-createhk.gov.hk/flipbook.php?lang=Chi&f=04)/[PDF](https://www.fso-createhk.gov.hk/pdf/Chap04-Chi04.pdf)
第05章 參考資料|[FSO](https://www.fso-createhk.gov.hk/flipbook.php?lang=Chi&f=05)/[PDF](https://www.fso-createhk.gov.hk/pdf/Chap05-Chi05.pdf)
第06章 聯絡資料|[FSO](https://www.fso-createhk.gov.hk/flipbook.php?lang=Chi&f=06)/[PDF](https://www.fso-createhk.gov.hk/pdf/Chap06-Chi06.pdf)
第07章 香港景致|[FSO](https://www.fso-createhk.gov.hk/flipbook.php?lang=Chi&f=07)/[PDF](https://www.fso-createhk.gov.hk/pdf/Chap07-Chi07.pdf)


### HK Film Industry Info

[創意香港][hk_create]官網提供一些[刊物](https://www.createhk.gov.hk/tc/publication.php)，其中包括 **香港電影業資料彙編** (A Collection of Information about Hong Kong Film Industry)。[香港電影發展局][hk_fdc](Hong Kong Film Development Council)官網亦提供該[刊物](https://www.fdc.gov.hk/tc/press/publication.htm)下載。

《香港電影業資料彙編》是[創意香港][hk_create] [電影服務統籌科][hk_create_fso]委托香港影業協會進行的一項電影資料搜集工作。彙編收集了有關各年度香港電影業的相關資料,覆蓋開拍的電影、本地上映、境外發行、獲頒獎項、年度加入的新導演及新演員、以及從業人員數目等範圍。是項資料搜集的目標是提供電影業界的資料和市場資訊予香港電影發展局成員及業界人士參考,讓他們知悉年內香港電影市場的表現及香港電影工業的整體情況。同時每一年度彙編資料的總結,可給香港電影發展局成員及業界人士提供一個了解香港電影年度變化和發展的途徑。

香港電影業資料彙編 (Archive備份)


Year|創意香港|香港電影發展局
---|---|---
2018|[PDF](https://www.createhk.gov.hk/publication/HKFI_Info_2018.pdf)|[PDF](https://www.fdc.gov.hk/doc/tc/HK_Film_Industry_Info2018_Preview.pdf)
2017|[PDF](https://www.createhk.gov.hk/publication/2017.pdf)|[PDF](https://www.fdc.gov.hk/doc/tc/HK_Film_Industry_Info2017_Preview.pdf)
2016|[PDF](https://www.createhk.gov.hk/publication/secondary_data_research_2016.pdf)|[PDF](https://www.fdc.gov.hk/doc/tc/HK_Film_Industry_Info2016_Preview.pdf)
2015|[PDF](https://www.createhk.gov.hk/publication/2015.pdf)|[PDF](https://www.fdc.gov.hk/doc/tc/HK_Film_Industry_Info2015_Preview.pdf)
2014|[PDF](https://www.createhk.gov.hk/publication/2014.pdf)|[PDF](https://www.fdc.gov.hk/doc/tc/HK_Film_Industry_Info2014_Preview.pdf)
2013|[PDF](https://www.createhk.gov.hk/publication/2013.pdf)|[PDF](https://www.fdc.gov.hk/doc/tc/HK_Film_Industry_Info2013_Preview.pdf)
2012|[PDF](https://www.createhk.gov.hk/publication/2012.pdf)|[PDF](https://www.fdc.gov.hk/doc/tc/HK_Film_Industry_Info2012_Preview.pdf)


[香港電影發展局][hk_fdc]官網同時提供了如下[刊物](https://www.fdc.gov.hk/tc/press/publication.htm)：

* [新世代監製介紹](https://www.fdc.gov.hk/NewAction/files/tc/doc/press/publication/Producer.pdf) (PDF)
* [新世代導演介紹](https://www.fdc.gov.hk/NewAction/files/tc/doc/press/publication/Director.pdf) (PDF)


## Hong Kong File Critics Society

香港電影評論協會

[香港電影評論協會][filmcritics]出版有季刊 [HKinema](https://www.filmcritics.org.hk/taxonomy/term/3/1)，並提供電子版PDF檔案下載。(Archive備份)

最新一期是 [第54號：合拍，唔合拍](https://www.filmcritics.org.hk/zh-hant/node/2903) (2021-06-02)。
<!-- 最新一期是 [第46號：邁向廿五周年 回顧與展望](https://www.filmcritics.org.hk/node/2481) (2019-03-13)。 -->

HKinema 詳細列表見[頁面](./HongKongFilmCriticsSociety/HKinema)。([script](./Script/README.md#hkinema))

## Hong Kong Film Archive

黃曉恩博士撰寫的『[香港電影資料館多面體——百年電影故事側寫](./History/當代史學第9卷第1期%282008.03%29.pdf)』詳細介紹了[香港電影資料館][filmarchive]，該文刊登在 [香港浸會大學歷史系](https://www.hkbu.edu.hk) 主辦的期刊『[當代史學](https://histweb.hkbu.edu.hk/newsletters_journals/detail/12/ 'Contemporary Historical Review')』(deprecated [link](http://histweb.hkbu.edu.hk/contemporary/contem.html)) [第9卷第1期(2008年3月)](https://histweb.hkbu.edu.hk/upload/newsletter_journal/120/issue/5f599b5a7eb2d.pdf) (deprecated [link](http://histweb.hkbu.edu.hk/contemporary/no.33.pdf))。

香港電影資料館官方網址:

1. [filmarchive.gov.hk][filmarchive]
    * 研究及出版[頁面](https://www.filmarchive.gov.hk/zh_TW/web/hkfa/rp.html)(2021年) 涵蓋 *出版刊物*、*電子刊物*、*資料館修復珍藏*、*通訊*季刊等。
    * 藏品及資源[頁面](https://www.filmarchive.gov.hk/zh_TW/web/hkfa/resources.html)(2021年) 涵蓋 *館藏目錄搜索*、*香港電影檢索*、*香港影人小傳檢索*。
2. [lcsd.gov.hk](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/home.html)
    * 研究及出版[頁面](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp.html) (2021年) 涵蓋 *出版刊物*、*電子刊物*、*資料館修復珍藏*、*通訊*季刊等。
    * 藏品及資源[頁面](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/resources.html)(2021年) 涵蓋 *館藏目錄搜索*、*香港電影檢索*、*香港影人小傳檢索*。

出版刊物

* [香港影片大全系列](./HongKongFilmArchive/hkfilmographyseries.md) (Hong Kong Filmography Series)
* [香港影人口述歷史叢書](./HongKongFilmArchive/oralhistoryseries.md) (Oral History Series)

### Film Maker

香港影人小傳

* [香港影人小傳](./HongKongFilmArchive/filmmaker.md) (Hong Kong Filmmakers)

### E-publications

電子刊物 (Archive備份)。

官方網址:

1. [filmarchive.gov.hk](https://www.filmarchive.gov.hk/zh_TW/web/hkfa/rp-electronic-publications-list.html)
2. [lcsd.gov.hk](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-electronic-publications-list.html)

Newsletter 詳細列表見[頁面](./HongKongFilmArchive/e-publications)。

### Newsletter

Newsletter (通訊) 平均每3個月一期，爲季刊。(Archive備份)

官方網址:

1. [filmarchive.gov.hk](https://www.filmarchive.gov.hk/zh_TW/web/hkfa/rp-newsletter.html)
2. [lcsd.gov.hk](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/rp-newsletter.html)


<!-- * [通訊 70~ 期](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/nletter.html) -->
<!-- * [通訊 (舊日通訊) 01~69期](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/nletterpast.html) -->

Newsletter 詳細列表見[頁面](./HongKongFilmArchive/newsletter)。([script](./Script/README.md#newsletter))


## Shaw Brothers Film

邵氏電影

[香港電影資料館][filmarchive]列有 [邵氏電影初探 - 邵氏大事記](https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/publications_souvenirs/pub/englishbooks/englishbooks_detail06/englishbooks_shawstory.html) 一文。

邵氏電影片庫([Shaw Brothers Film Libary](https://en.wikipedia.org/wiki/Celestial%27s_Shaw_Brothers_Film_Library))現歸[天映娛樂][celestialpictures](Celestial Pictures)所有，[天映娛樂][celestialpictures]總部設於香港，是 Astro Overseas Limited 的全資附屬公司。

天映娛樂擁有邵氏電影之全球永久版權，版權涵蓋電影院播放、多媒體家庭娛樂發行、衍生產品製作及相關之版權買賣，以及重拍電影或拍攝續集等。具體介紹見其官網 [公司簡介](http://www.celestialpictures.com/company-tc.asp)，[邵氏片庫 - 背景](http://www.celestialpictures.com/co-shaw-story-tc.asp)。

>Celestial Pictures owns worldwide rights, across all media, in perpetuity including the rights for theatrical releases, multi-media distribution, merchandising and ancillary rights to the Shaw Brothers Library.

[天映娛樂][celestialpictures]月2009年4月30日(週四)將邵氏片庫珍貴典藏予[香港電影資料館][filmarchive]收藏，具體見[新聞公報](https://www.info.gov.hk/gia/general/200904/30/P200904300173.htm)。

邵氏電影片庫更多介紹見[天映娛樂][celestialpictures]官網 [Shaw Brothers Film Libary](http://www.celestialpictures.com/co-shaw.asp)。如果需要查看影片的影碟封面圖片，可在 [We♥Movies](http://movies.weheartmusic.com/celestial-pictures/celestial-pictures-hong-kong) 查看。

### Film List

[香港電影資料館][filmarchive]收錄邵氏參與的影片 [1310](./ShawBrothers/FilmList/香港電影資料館_邵氏參與影片一覽表.md '香港電影資料館_邵氏參與影片一覽表') 部，出品的影片 [882](./ShawBrothers/FilmList/香港電影資料館_邵氏影片一覽表.md '香港電影資料館_邵氏影片一覽表') 部，公司名稱有 `邵氏兄弟(香港)有限公司`、`邵氏父子有限公司`、`邵氏父子(一九六八)有限公司`、`邵氏公司`、`邵氏影城香港有限公司`。可通過官方提供的檢索工具 [香港電影檢索 (進階)	](https://ipac.hkfa.lcsd.gov.hk/ipac/AS/AS-002-02_Tc.zul)進行查詢。

[香港記憶][hkmemory]網站收藏有 **741** 部。

[香港影庫][hkmdb]網站收錄 [邵氏兄弟有限公司](http://hkmdb.com/db/companies/view.mhtml?id=506&display_set=big5)作品 **1449** 部，[邵氏兄弟(香港)有限公司](http://hkmdb.com/db/companies/view.mhtml?id=1388&display_set=big5)作品 **83** 部。

光影久久論壇楓華整理的邵氏影片清單有 [877](./ShawBrothers/FilmList/光影久久楓華_邵氏影片一覽表.md '光影久久楓華_邵氏影片一覽表') 部 (截至2003年 [xls](./ShawBrothers/FilmList/光影久久楓華_邵氏影片一覽表.xls '光影久久楓華_邵氏影片一覽表'))。

豆瓣網有網友整理邵氏影片 [851](https://www.douban.com/doulist/491994/) 部，資料來自[香港電影資料館][filmarchive]。


### Genres

邵氏電影可以按片種分爲若干個分類，[天映娛樂][celestialpictures]在其官網的 [Shaw Brothers Film Library](http://www.celestialpictures.com/co-shaw.asp) 頁面提供 [The Complete Celestial’s Shaw Brothers Film Collection](http://www.celestialpictures.com/doc/sb-booklet.zip) 下載，壓縮包中共12個PDF文件，按照片種列出影片。

以下爲具體片種

片種|英文|數量|We♥Movies|Film Collection Booklet (PDF)
---|---|---|---|---
喜劇片 | Comedy | 45 | [Comedy](http://movies.weheartmusic.com/celestial-pictures/celestial-pictures-hong-kong/comedy) | [Comedy](./ShawBrothers/sb-booklet/06%20Comedy.pdf)
文藝片 | Drama | 90 | [Drama](http://movies.weheartmusic.com/celestial-pictures/celestial-pictures-hong-kong/drama) | [Drama](./ShawBrothers/sb-booklet/07%20Drama.pdf)
艷情片 | Erotica | 24 | [Erotica](http://movies.weheartmusic.com/celestial-pictures/celestial-pictures-hong-kong/erotic) / [Exploitation](http://movies.weheartmusic.com/celestial-pictures/celestial-pictures-hong-kong/exploitation-ivl) | [Erotica](./ShawBrothers/sb-booklet/04%20Erotica.pdf)
夢幻片 | Fantasy | 46 | | [Fantasy](./ShawBrothers/sb-booklet/12%20Fantasy.pdf)
恐怖片 | Horror | 17 | [Horror](http://movies.weheartmusic.com/celestial-pictures/celestial-pictures-hong-kong/horror) | [Horror](./ShawBrothers/sb-booklet/05%20Horror.pdf)
驚憟片 | Thriller | 5 | | [Thriller](./ShawBrothers/sb-booklet/11%20Thriller.pdf)
黃梅調 | Huangmei Opera | 75 | | [Huangmei Opera](./ShawBrothers/sb-booklet/08%20Humangmei%20opera.pdf)
功夫片 -武術片／動作片 | Kung Fu – Martial Arts / Action | 307 / 48 | [Martial Arts](http://movies.weheartmusic.com/celestial-pictures/celestial-pictures-hong-kong/martial-arts), [Action](http://movies.weheartmusic.com/celestial-pictures/celestial-pictures-hong-kong/action) | [Action](./ShawBrothers/sb-booklet/01%20Action.pdf) / [Martial Art (1)](./ShawBrothers/sb-booklet/02%20Martial%20Art%20%281%29.pdf) / [Martial Art (2)](./ShawBrothers/sb-booklet/03%20Martial%20Art%20%282%29.pdf)
歌舞片 | Musical | 43 | [Musical](http://movies.weheartmusic.com/celestial-pictures/celestial-pictures-hong-kong/musical) | [Muscial](./ShawBrothers/sb-booklet/09%20Muscial.pdf)
古裝宮闈片 | Period Drama | 41 | | [Period Drama](./ShawBrothers/sb-booklet/10%20Period%20Drama.pdf)

**說明**：各片種數量數據來自[香港記憶][hkmemory]的專題特藏——[邵氏電影][hkmemory_shaw_brothers_movies] (數據似乎不正確)，該專題列有電影海報。


## Filmology

香港電影相關研究

* [港日影人口述歷史](https://hkupress.hku.hk/pro/con/1073.pdf)


## Miscellaneous

雜七雜八

[個人電影彙集(2014.01.13)](./Miscellaneous/個人電影彙集%282014.01.13%29.xlsx)

音樂

* [中國流行音樂第一人](./Miscellaneous/中國流行音樂第一人.md)

電影

* [嘉禾出品電影目錄](./Miscellaneous/嘉禾出品電影目錄.txt)
* [香港電影七大類型片](./Miscellaneous/香港電影七大類型片.md)
* [關於華語電影存儲媒介之分區及一些基本常識](./Miscellaneous/關於華語電影存儲媒介之分區及一些基本常識.md)
* [香港电影的秘密--香港电影书籍推荐](./Miscellaneous/香港电影的秘密--香港电影书籍推荐.md)


## Chnage Log

* Feb 25, 2019 16:59 Mon ET
  * 初稿完成
* Mar 19, 2019 23:57 Tue ET
  * 排版更新
* Mar 26, 2019 13:59 Tue ET
  * 新增 [香港影業協會][hk_mpia] 及 《香港電影業資料彙編》
* Aug 14, 2021 21:00 Sat ET
  * 更新鏈接，修復Shell腳本


[hk_mpia]:http://mpia.org.hk "香港影業協會"
[hk_fdc]:https://www.fdc.gov.hk "香港電影發展局"
[hk_create]:https://www.createhk.gov.hk "創意香港"
[hk_create_fso]:http://fso-createhk.gov.hk "Film Services Office, Create Hong Kong"
[mow]:https://en.unesco.org/programme/mow "Memory of the World"
[hongkongcultures]:https://hongkongcultures.blogspot.com "香港文化資料庫"
[hkmemory]:https://www.hkmemory.hk '香港記憶'
[hkmemory_shaw_brothers_movies]:https://www.hkmemory.hk/collections/shaw_brothers_movies/about/ '邵氏電影'
[celestialpictures]:http://www.celestialpictures.com '天映娛樂'
[filmcritics]:https://www.filmcritics.org.hk '香港電影評論協會'
[filmarchive]:https://www.filmarchive.gov.hk '香港電影資料館'
<!-- https://www.lcsd.gov.hk/CE/CulturalService/HKFA/zh_TW/web/hkfa/home.html -->
[hkmdb]:http://hkmdb.com '香港影庫'


<!-- End -->
